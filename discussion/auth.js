const jwt = require ("jsonwebtoken")
const User = require("./models/User")
const Course = require("./models/Course")
const secret = "IDoNotKnow" // form of message/string in which this will serve as our secret code

//JSON WebToken - is a  way of securely passing of information form a part of server to the front end
// information is kept secure throught the use of seceret variable
// the secret variable is only known by the system/browser which can be decoded by the server

// token creation 
module.exports.createAccessToken = (user) => {
    // data will be receive from the registration format, when the token login, a token will be created with the user's information (this information is still encrypted inside the token)

    //  these are the data that needs to be authenticated
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }
    // generates the token using the form data and the secret code without additional options
    return jwt.sign(data, secret, {})
}

// TOKEN VERIFICATION 
/* module.exports.verify = (req, res, next) => { //next paramater tells the server to proceed if the verification is ok / successful
    let token = req.headers.authorization //athorization can be found in the headers of the request and tells wether the client / user has the authority to send the request
    if (typeof token !== "undefined") { //token is present
        console.log(token)
        token = token.slice(7, token.length)


        // this verifies the token using the secret, and fails if the token's secret does not match the secret variable meaning there is an attempt to tamper the data from the user-end
        return jwt.verify(token, secret, (err, data) => {
            if(err){
                return res.send({auth: "failed"})
            } else {
                // tells teh server to proceed to processing of the request
                next();
            }
        })
    }
} */

module.exports.verify = (req, res, next) => { //next parameter - tells the server to proceed if the verification is okay/successful
	let token = req.headers.authorization //authorization can be found in the headers of the request and tells whether the client/user has the authority to send the request

	if (typeof token !== "undefined") { //token is present
		console.log(token)
		token = token.slice(7, token.length)
// jwt.verify - verifies the token using the secret, and fails if the token's secret does not match the secret  variable meaning there is an attempt hack, or atleast to tamper/change the data from the user-end
		return jwt.verify(token, secret, (err, data) =>{
			if(err){
				return res.send({auth: "failed"})
			} else {
				// tells the server to proceed to processing of the request
				next();
			}
		})
	}
}





// TOKEN DECRYPTION / DECODING
/* module.exports.decode = (token) => {
    if (typeof token !== "undefined") { //token is present

        token = token.slice(7, token.length)

        return jwt.verify(token, secret, (err, data) => {
            if (err){
                return null
            } else {
                return jwt.decode(token, {complete: true}).payload
                // jwt.decode - deciddes the data to be decoded, which is the token
                // payload is the one that we need to verify the user information, this is part of the token when we code(d) the createAccessToken function (the one with _id, email, and isAdmin)
            }
        })   
    }
    else {
        return null //there is no token
    }
} */


module.exports.decode = (token) => {
	if (typeof token !== "undefined") { //token is present
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				console.log(err)
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload
				// jwt.decode - decides the data to be decoded, which is the token
				// payload - is the one that we need to verify the user information this is a part of the token when code the createAccessToken function (the on with _id, email, and isAdmin)
			}
		})
	} else {
		return null //there is no token
	}
} 
