/* 
Activity:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Create a git repository named S34.
4. Add another remote link and push to git with the commit message of Add activity code.
5. Add the link in Boodle.

*/

const User = require("../models/User.js")
const Course = require("../models/Course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")
const { request } = require("express")




// if isAdmin === true then addCourse

 
module.exports.addCourse = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin === false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course ({
                name : requestBody.name,
                description : requestBody.description,
                price : requestBody.price,
                isActive : requestBody.isActive,
                createdOn : requestBody.createdOn,
            })
            return newCourse.save().then((saved, error)=>{
                if (error){
                    console.log(error)
                    return false
                } 
                else {
                    return true
                }
            })
        }
        
    });    
}

