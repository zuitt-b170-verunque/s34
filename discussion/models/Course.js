const mongoose = require ("mongoose")

const courseSchema = new mongoose.Schema({

    name:{
        type:String,
        required: [true, "Course Name is required"]
    },

    description:{
        type:String,
        required:[true, "Course description is required"]
    },

    // MINI ACTIVITY
    /* create a similar data structure for 
    price : (number), 
    isActive : (boolean), 
    createdOn(type:date, default value: new date) 
    */
    price:{
        type:Number,
        required: [true, "Course Name is required"]
    },

    isActive:{
        type:Boolean,
        default : true
    },

   createdOn : {
       type : Date,
       default : new Date()
   },

   enrollees : [
       {
           userId : {
               type : String,
               required : [true, "User ID is required"]
           },

           enrolledOn : {
               type : Date,
               default : new Date()
           }
       }
   ]
})

module.exports = mongoose.model("Course", courseSchema)